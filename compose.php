<?php include('includes/server.php');?>
<?php include('header.php') ;?>
<?php
$file='mail';
$files='management';
$result = $db->query('SELECT * FROM users')->fetchAll();?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Compsoe Email</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Compose Email</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="tabbable-line">
								<div class="tab-content">
									<div class="tab-pane active fontawesome-demo" id="tab1">
										<div class="row">
											<div class="col-md-12">
												<div class="card card-topline-red">
													<div class="card-body ">
														 <div class="inbox-body">
															<div class="inbox-header">
															</div>
															<div class="inbox-body no-pad">
																<div class="mail-list">
																	<div class="compose-mail">
																		<form method="post" name="compose" action="compose_mail.php" id="compose" enctype="multipart/form-data">
																			<div class="form-group">
																				<label for="to" class="">To:</label>
																				<input name="to" type="text" tabindex="1" id="to" class="form-control">
																				<div class="compose-options">
																					<a onclick="$(this).hide(); $('#cc').parent().removeClass('hidden'); $('#cc').focus();" href="javascript:;">Cc</a>
																					<a onclick="$(this).hide(); $('#bcc').parent().removeClass('hidden'); $('#bcc').focus();" href="javascript:;">Bcc</a>
																				</div>
																			</div>
																			<div class="form-group hidden">
																				<label for="cc" class="">Cc:</label>
																				<input  name="cc" type="text" tabindex="2" id="cc" class="form-control">
																			</div>
																			<div class="form-group hidden">
																				<label for="bcc" class="">Bcc:</label>
																				<input name="bcc" type="text" tabindex="2" id="bcc" class="form-control">
																			</div>
																			<div class="form-group">
																				<label for="subject" class="">Subject:</label>
																				<input name="subject" type="text" tabindex="1" id="subject" class="form-control">
																			</div>
																			<div class="compose-editor">
																				<textarea id="summernote" name="email_body" class="summernote"></textarea>
																				<input type="file" name="file" id="file" class="default" >
																			</div>
																			<div class="btn-group margin-top-20 ">
																				<button  type="submit" name="sendall" id="sendall" class="btn btn-primary btn-sm margin-right-10"><i class="fa fa-check"></i> Send</button>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
		
<script>
$(document).ready(function(){
	$( "#sendall" ).click(function( event ) {
		var to = $("#to").val().trim();
		var subject = $("#subject").val().trim();
		var summernote = $("#summernote").val().trim();
		if(to.length == 0) {
			alert("Enter To Address");
			return false;
		}
		if(subject.length == 0) {
			alert("Enter Subject");
			return false;
		}
		if(summernote.length == 0) {
			alert("Enter Message Body");
			return false;
		}
		
		if($('#file').val().length > 0) {
			var file = $('#file')[0].files[0];
			var file_size = $('#file')[0].files[0].size / 1024 / 1024;
			var file_type = $('#file')[0].files[0].type;
			console.log($('#file')[0].files[0]);
			var ext = $('#file').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg','pdf']) == -1) {
				alert('invalid extension! Upload only jpg, gif, png, or pdf');
				return false;
			}
			if(file_size > 2) {
				alert("File size exceeds 2 MB");
				return false;
			}

		} else {
			//return false;
		}
		return true;
		//event.preventDefault();
	});
	$('.mybutton').click(function(){
		var ID = $(this).data('id');
		$('#confirm-button').data('id', ID); //set the data attribute on the modal button
		showCancelMessage(ID);
	});
	
	function showCancelMessage(deleteid) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this user!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plx!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
				var el = $("#mybutton_"+deleteid);
					$.ajax({
						url: 'deluser.php',
						type: 'POST',
						data: { id:deleteid },
						success: function(response){
							// Removing row from HTML Table
							if(response == 1){
								$(el).closest('tr').css('background','tomato');
								$(el).closest('tr').fadeOut(800,function(){
									//$('#confirm_delete_modal').modal('hide');
								});
							}else{
								alert(response);
							}

						}
					});
				swal("Deleted!", "Your user  has been deleted.", "success");
			} else {
				swal("Cancelled", "Your user  is safe :)", "error");
			}
		});
	}
	$( "#to" ).on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( "searchuser.php", {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
	  
  function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
	  
});
$('#summernote').summernote({
	placeholder: '',
	tabsize: 2,
	height: 200,
});
</script>
<style>
.ui-draggable, .ui-droppable {
	background-position: top;
}
</style>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>