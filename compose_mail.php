<?php include('includes/server.php');
require 'phpmailer/src/PHPMailer.php'; 
require 'phpmailer/src/SMTP.php'; 
require 'phpmailer/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
if(isset($_POST['to'])) {
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->Host = $smtp_detail['mailer_host'];
	$mail->SMTPAuth = true;
	$mail->Username = $smtp_detail['mailer_user']; 
	$mail->Password = $smtp_detail['mailer_password'];
	$mail->SMTPSecure = $smtp_detail['mailer_encryption'];
	$mail->Port = $smtp_detail['mailer_port'];
	$mail->AuthType = $smtp_detail['mailer_auth_mode'];
	$mail->Subject = $_POST['subject'];
	$mail->setFrom($smtp_detail['mailer_from_email'], $smtp_detail['mailer_from_name']);
	$to = explode(",", $_POST['to']);
	
	if(!empty($_POST['cc'])) {
		$cc = explode(",", $_POST['cc']);
		foreach($cc as $key => $value) {
			$mail->addCC($value, '');
		}
	}
	if(!empty($_POST['bcc'])) {
		$bcc = explode(",", $_POST['bcc']);
		foreach($bcc as $key => $value) {
			$mail->addBCC($value, '');
		}
	}
	
	$mail->isHTML(true);
	if($_FILES['file']) {
		$target_dir = getcwd()."/assets/files/";
		$file_name = date("ymdhis")."_".basename($_FILES["file"]["name"]);
		$target_file = $target_dir . $file_name;
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
			$mail->addAttachment($target_dir.$file_name, $_FILES["file"]["name"]);
		}
	}
	foreach($to as $key => $value) {
		$email = trim($value);		
		if($email){
			$result = $db->query('SELECT * FROM users where email = "'.$email.'" ')->fetchArray();
			$full_name = null;
			$user_id = 0;
			if(isset($result['id'])) {
				$user_id = $result['id'];
				$full_name = $result['first_name'] . ' '. $result['last_name'];
			}
			
			$mail->addAddress($email, $full_name);
			$mail->Body = $_POST['email_body'];
			if($mail->send()){
				$insert = $db->query('INSERT INTO sentemail (user_id,email,subject,body,date_added,email_sent,sent_by) VALUES (?,?,?,?,?,?,?)', $user_id, $email, $_POST['subject'], $_POST['email_body'],date("Y-m-d H:i:s"),"1",$_SESSION['userData']['id']);
			} else {
				$insert = $db->query('INSERT INTO sentemail (user_id,email,subject,body,date_added,email_sent,sent_by) VALUES (?,?,?,?,?,?,?)', $user_id, $email, $_POST['subject'], $_POST['email_body'],date("Y-m-d H:i:s"),"0",$_SESSION['userData']['id']);
			}
			$mail->clearAddresses();
		}
	}
	header('location:mail.php');
	exit;
} else {
	header('location:mail.php');
	exit;
}