<?php
include ('includes/server.php');
if(isset($_POST['view'])){
	if($_POST["view"] != '')
	{
		$update_query = "UPDATE audit_log SET isseen = '1' WHERE isseen='0' ";
		$db->query($update_query);
	}
	$result_notication = $db->query("SELECT audit_log.date_added,audit_log.object_id,audit_log.user_name,audit_log.action,audit_log.user_id,audit_log.id as nid FROM audit_log inner join users on audit_log.object_id = users.id where bundle = 'user' and object in ('security','user') and action in ('login','create')  order by audit_log.id desc limit 5")->fetchAll();

	$output = "";
	if($result_notication) {
		foreach($result_notication as $key => $value) {
			$result_data = $db->query("SELECT username,email FROM users where id='".$value['object_id']."'")->fetchArray();
		$output .='<li>
			<a href="javascript:;">
				<span class="time">'.time_Ago(strtotime($value['date_added'])).'</span>
				<span class="details">
					<span class="notification-icon circle purple-bgcolor"><i class="fa fa-user o"></i></span>
					'.$result_data['username'].'&nbsp;'.$value['action'].'</span>
			</a>
		</li>';
		 }
		 
	 } else {
		$output .='<li>
			<a href="#" class="text-bold text-italic">No Notification Found</a>
		</li>'; 
	 }
	$result_notication = $db->query("SELECT audit_log.date_added,audit_log.object_id,audit_log.user_name,audit_log.action,audit_log.user_id,audit_log.id as nid FROM audit_log inner join users on audit_log.object_id = users.id where bundle = 'user' and object in ('security','user') and action in ('login','create')  and isseen='0' order by audit_log.id ");
	$data = array(
	   'notification' => $output,
	   'unseen_notification'  => $result_notication->numRows()
	);
	echo json_encode($data);
}

function time_Ago($time) { 
  
    // Calculate difference between current 
    // time and given timestamp in seconds 
    $diff     = time() - $time; 
      
    // Time difference in seconds 
    $sec     = $diff; 
      
    // Convert time difference in minutes 
    $min     = round($diff / 60 ); 
      
    // Convert time difference in hours 
    $hrs     = round($diff / 3600); 
      
    // Convert time difference in days 
    $days     = round($diff / 86400 ); 
      
    // Convert time difference in weeks 
    $weeks     = round($diff / 604800); 
      
    // Convert time difference in months 
    $mnths     = round($diff / 2600640 ); 
      
    // Convert time difference in years 
    $yrs     = round($diff / 31207680 ); 
      
    // Check for seconds 
    if($sec <= 60) { 
        return "$sec seconds ago"; 
    } 
      
    // Check for minutes 
    else if($min <= 60) { 
        if($min==1) { 
            return "one minute ago"; 
        } 
        else { 
            return "$min minutes ago"; 
        } 
    } 
      
    // Check for hours 
    else if($hrs <= 24) { 
        if($hrs == 1) {  
            return "an hour ago"; 
        } 
        else { 
            return "$hrs hours ago"; 
        } 
    } 
      
    // Check for days 
    else if($days <= 7) { 
        if($days == 1) { 
            return "Yesterday"; 
        } 
        else { 
            return "$days days ago"; 
        } 
    } 
      
    // Check for weeks 
    else if($weeks <= 4.3) { 
        if($weeks == 1) { 
            return "a week ago"; 
        } 
        else { 
            return "$weeks weeks ago"; 
        } 
    } 
      
    // Check for months 
    else if($mnths <= 12) { 
        if($mnths == 1) { 
            return "a month ago"; 
        } 
        else { 
            return "$mnths months ago"; 
        } 
    } 
      
    // Check for years 
    else { 
        if($yrs == 1) { 
            return "one year ago"; 
        } 
        else { 
            return "$yrs years ago"; 
        } 
    } 
} 
?>