		<!-- start footer -->
		<div class="page-footer">
			<div class="page-footer-inner"> All Rights Reserved &copy; Mautic AI | Developed By
				<a href="mailto:info@vbeasy.com" target="_top" class="makerCss">VB Easy</a>
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<!-- end footer -->
	</div>
	<!-- start js include path -->
	<script src="../assets/jquery.min.js"></script>
	<script src="../assets/popper/popper.js"></script>
	<script src="../assets/jquery.blockui.min.js"></script>
	<script src="../assets/jquery.slimscroll.js"></script>
	<!-- bootstrap -->
	<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="../assets/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../assets/summernote/summernote.js"></script>
	<!-- data tables -->
    <script src="../assets/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
	<script src="../assets/datatables/export/dataTables.buttons.min.js"></script>
	<script src="../assets/datatables/export/buttons.flash.min.js"></script>
	<script src="../assets/datatables/export/jszip.min.js"></script>
	<script src="../assets/datatables/export/pdfmake.min.js"></script>
	<script src="../assets/datatables/export/vfs_fonts.js"></script>
	<script src="../assets/datatables/export/buttons.html5.min.js"></script>
	<script src="../assets/datatables/export/buttons.print.min.js"></script>
	<!-- Common js-->
	<script src="../assets/app.js"></script>
	<script src="../assets/layout.js"></script>
	<script src="../assets/theme-color.js"></script>
	<!-- material -->
	<script src="../assets/material/material.min.js"></script>
	<!-- chart js -->
	<script src="../assets/chart-js/Chart.bundle.js"></script>
	<script src="../assets/chart-js/utils.js"></script>
	<?php if($file == 'index') { ?>
		<script src="../assets/apexcharts/apexcharts.min.js"></script>
	<?php } ?>
	<script src="../assets/sweet-alert/sweetalert.min.js"></script>
	<!-- end js include path -->
	<script>
		function load_unseen_notification(view = '') {
			$.ajax({
				url:"fetch.php",
				method:"POST",
				data:{view:view},
				dataType:"json",
				success:function(data)
				{
					$('.dropdown-menu-list').html(data.notification);
					if(data.unseen_notification > 0)
					{
						$('.count').html(data.unseen_notification);
						$('.count').show();
						$('.countinner').html('New '+ data.unseen_notification);
					} else {
						$('.count').hide();
					}
				}
			});
		}
		load_unseen_notification();
		$(document).on('click', '.dropdown-toggle-noti', function(){
		$('.count').html('');
			load_unseen_notification('yes');
		});
		setInterval(function(){
			load_unseen_notification();
		}, 5000);
	</script>
</body>


</html>