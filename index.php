<?php include('includes/server.php');
$description = '';
$author = '';
$title = '';
include('header.php');
$file = 'index';
$files = 'dashboard'; 
$result = $db->query('SELECT * FROM users order by date_added limit 10')->fetchAll();?>


		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Dashboard</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Dashboard</li>
							</ol>
						</div>
					</div>
					
					<!-- chart start -->
					
					<div class="row">
						
						<div class="col-md-6">
							<div class="card card-box">
								<div class="card-head">
									<header>Total Users</header>
									<div class="tools">
										<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
										<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
									</div>
								</div>
								<div class="card-body">
									<div class="recent-report__chart">
										<div id="chart_line"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card card-box">
								<div class="card-head">
									<header>Active Users</header>
									<div class="tools">
										<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
										<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
										<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
									</div>
								</div>
								<div class="card-body no-padding height-9">
									<div class="row">
										<canvas id="chartjs_bar"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="card card-topline-lightblue">
								<div class="card-head">
									<header>User Types</header>
									<div class="tools">
										<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
										<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
										<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
									</div>
								</div>
								<div class="card-body " id="chartjs_pie_parent">
									<div class="row">
										<canvas id="chartjs_pie" height="120"></canvas>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card  card-box">
								<div class="card-head">
									<header>Users List</header>
									<div class="tools">
										<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
										<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
									</div>
								</div>
								<div class="card-body ">
									<div class="table-wrap">
										<div class="table-responsive">
											<table class="table display product-overview mb-30" id="support_table">
												<thead>
													<tr>
														<th>Id</th>
														<th>Name</th>
														<th>Membership</th>
														<th>Email</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php
													if($result) {
														foreach ($result as $data) {
													?>
													<tr>
														<td><?php echo $data['id'];?></td>
														<td><?php echo $data['first_name'].' '.$data['last_name'];?></td>
														<td>
															<?php echo ($data['stripeid']) != '' ? '<span class="label label-sm label-success">paid</span>' : '<span class="label label-sm label-warning">unpaid </span>' ;?>
														</td>
														<td><?php echo $data['email'];?></td>
														<td> 
													<a data-payment="<?php echo $data['amountpaid'];?>" href="javascript:void(0);" data-name="<?php echo $data['first_name'].' '.$data['last_name'];?>" data-user = "<?php echo $data['first_name'].' '.$data['last_name']. ' '.$data['email'].' ';?>" data-id="<?php echo $data['id'];?>"  data-toggle="modal" data-target="#sendemail" class="sendemail  ">
																					<i class="fa fa-send "></i></a>
																					<?php if($data['id'] !=1) { ?>
															<a  href="javascript:void(0);" id = "mybutton_<?php echo $data['id'];?>"   data-id="<?php echo $data['id'];?>"  class="mybutton   ">
																					<i class="fa fa-trash-o "></i>
																				</a>
																					<?php } ?>
																				</td>
													</tr>
													<?php } ?>
													<?php } ?> 
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Chart end -->
				</div>
			</div>
			<!-- end page content -->
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>		
<div class="modal fade" id="sendemail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
 aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLongTitle">Send Email</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="inbox-body">
					<div class="inbox-header"></div>
					<div class="inbox-body no-pad">
						<div class="mail-list">
							<div class="compose-mail">
								<form method="post" id="sent_email" name="sent_email">
									<div class="form-group">
										<label for="to" class="">To:</label>
										<input type="hidden" value="" name="to_user" id="to_user" />
										<input type="hidden" value="" name="to_user_id" id="to_user_id" />
										<span class="compose_to"></span>
									</div>
									<div class="form-group">
										<label for="subject" class="">Subject:</label>
										<input type="text" value="" tabindex="1" id="subject" name="subject" class="form-control">
									</div>
									<div class="compose-editor">
										<textarea id="summernote" name="email_body" class="summernote"></textarea>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="sendemailuser" class="btn btn-primary">Send</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('.mybutton').click(function(){
		var ID = $(this).data('id');
		$('#confirm-button').data('id', ID); //set the data attribute on the modal button
		showCancelMessage(ID);
	});

	$('.sendemail').click(function(){
		var ID = $(this).data('id');
		var to_user = $(this).data('user');
		var name = $(this).data('name');
		var payment = $(this).data('payment');
		$("#to_user").val(name);
		$("#to_user_id").val(ID);
		 
		$('#sendemailuser').data('id', ID); //set the data attribute on the modal button
		$('.compose_to').html('<strong>'+to_user+'</strong>');
		var msg = "Hello <strong>"+ name+"</strong>,<Br />";
		if(payment<=0) {
			$("#subject").val('Payment Due');
			msg += "Please make payment to see all your dashboard functionality.<br/>";
			msg += "Here is link to make payment <a target='_new' href='https://app.mauticai.com/stripe'> https://app.mauticai.com/stripe </a>.<br/><br/>";
		} else {
			$("#subject").val('');
		}
		msg += "Thanks.<br/>Mauticai Team,<b /><br/>";
		$('#summernote').val(msg);
		$('#summernote').summernote('destroy');
		$('#summernote').summernote({
			placeholder: '',
			tabsize: 2,
			height: 200,
		});
		
	});
	
	$('#sendemailuser').click(function(){
		var form = $("#sent_email");
		
		var subject = $("#subject").val().trim();
		var summernote = $("#summernote").val().trim();

		if(subject.length == 0) {
			alert("Enter Subject");
			return false;
		}
		if(summernote.length == 0) {
			alert("Enter Message Body");
			return false;
		}
		$.LoadingOverlay("show");
		
		$.ajax({
			url: 'sentemail.php',
			type: 'POST',
			data: form.serialize(),
			success: function(response){
				// Removing row from HTML Table
				if(response == 1){
					swal("Email Sent!", "Email sent to user.", "success");
					$("#sendemail").modal('hide');
				}else{
					$("#sendemail").modal('hide');
					swal("Cancelled", response, "error");
				}
				$.LoadingOverlay("hide",true);

			}
		});
	})
	
	function showCancelMessage(deleteid) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this user!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plx!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
				var el = $("#mybutton_"+deleteid);
					$.ajax({
						url: 'deluser.php',
						type: 'POST',
						data: { id:deleteid },
						success: function(response){
							// Removing row from HTML Table
							if(response == 1){
								$(el).closest('tr').css('background','tomato');
								$(el).closest('tr').fadeOut(800,function(){
									//$('#confirm_delete_modal').modal('hide');
								});
							}else{
								alert(response);
							}

						}
					});
				swal("Deleted!", "Your user  has been deleted.", "success");
			} else {
				swal("Cancelled", "Your user  is safe :)", "error");
			}
		});
	}
});
</script>
<?php
	$result = $db->query("SELECT count(*) as number,DATE_FORMAT(date_added,'%m') as month FROM users where DATE_FORMAT(date_added,'%Y') = '2020' group by  DATE_FORMAT(date_added,'%m') order by DATE_FORMAT(date_added,'%m')")->fetchAll();
	$data = array();
	$short = array('1'=>'"Jan"','2'=>'"Feb"','3'=>'"Mar"','4'=>'"Apr"','5'=>'"May"','6'=>'"Jun"','7'=>'"Jul"','8'=>'"Aug"','9'=>'"Sep"','10'=>'"Oct"','11'=>'"Nov"','12'=>'"Dec"');
	$final = array();
	$current = (int)date("m");
	for($i=1; $i<=12;$i++) {
		if($current>=$i) {
			$final[] = $short[$i];
		}
		$j = $i;
		if($i <=9) {
			$j = "0".$i;
		}
		//$data[$j] = rand(5,100);
		$data[$j] = 0;
		foreach($result as $key => $value) {
			$month = (int)$value['month'];
			if($i == $month) {
				$data[$j] = $value['number'];
			}
		}
	}
	$data = "[" . implode(",",$data). "]";
	
	$register_users = $db->query("SELECT id FROM users ");
	$register_users = $register_users->numRows();
	
	$unpaid_users = $db->query("SELECT id FROM users  where stripeid='' or stripeid is null ");
	$unpaid_users = $unpaid_users->numRows();
	
	$paid_users = $db->query("SELECT id FROM users  where stripeid !='' ");
	$paid_users = $paid_users->numRows();
	$final = "[" . implode(",",$final). "]";
	
	
	$result_active = $db->query("SELECT count(*) as number,DATE_FORMAT(last_active,'%m') as month FROM users where DATE_FORMAT(last_active,'%Y') = '2020' group by  DATE_FORMAT(last_active,'%m') order by DATE_FORMAT(last_active,'%m')")->fetchAll();
	$active_users = array();
	for($i=1; $i<=12;$i++) {
		$j = $i;
		if($i <=9) {
			$j = "0".$i;
		}
		//$data[$j] = rand(5,100);
		$active_users[$j] = 0;
		foreach($result_active as $key => $value) {
			$month = (int)$value['month'];
			if($i == $month) {
				$active_users[$j] = $value['number'];
			}
		}
	}
	$active_users = "[" . implode(",",$active_users). "]";
?>
<script>
		
chart_line();		
function chart_line() {
  var options = {
    chart: {
      height: 400,
      type: "line",
      shadow: {
        enabled: true,
        color: "#000",
        top: 18,
        left: 7,
        blur: 10,
        opacity: 1,
      },
      toolbar: {
        show: false,
      },
    },
    colors: ["#786BED", "#999b9c"],
    dataLabels: {
      enabled: true,
    },
    stroke: {
      curve: "smooth",
    },
    series: [
      {
        name: "Users - 2020",
        data: <?php echo $data;?>,
      },
    ],
    grid: {
      borderColor: "#e7e7e7",
      row: {
        colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
        opacity: 0.0,
      },
    },
    markers: {
      size: 6,
    },
    xaxis: {
      categories: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "July",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ],

      labels: {
        style: {
          colors: "#9aa0ac",
        },
      },
    },
    yaxis: {
      labels: {
        style: {
          color: "#9aa0ac",
        },
      },
      min: 10,
      max: 100,
    },
    legend: {
      position: "top",
      horizontalAlign: "right",
      floating: true,
      offsetY: -25,
      offsetX: -5,
    },
  };

  var chart = new ApexCharts(document.querySelector("#chart_line"), options);

  chart.render();
}




$(document).ready(function() {
  var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config = {
        type: 'pie',
    data: {
        datasets: [{
            data: [
                <?php echo $unpaid_users;?>,
                <?php echo $paid_users;?>,
                <?php echo $register_users;?>,
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.green
            ],
            label: 'Dataset 1'
        }],
        labels: [
            "Unpaid",
            "Paid",
            "Register Users"
        ]
    },
    options: {
        responsive: true
    }
};

    var ctx = document.getElementById("chartjs_pie").getContext("2d");
    window.myPie = new Chart(ctx, config);
});



$(document).ready(function() {
   var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
     var color = Chart.helpers.color;
     var barChartData = {
         labels: <?php echo $final; ?>,
         datasets: [{
             label: 'Total Users',
             backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
             borderColor: window.chartColors.red,
             borderWidth: 1,
             data: <?php echo $data;?>
         }, {
             label: 'Active Users',
             backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
             borderColor: window.chartColors.blue,
             borderWidth: 1,
             data: <?php echo $active_users;?>
         }]

     };

         var ctx = document.getElementById("chartjs_bar").getContext("2d");
         window.myBar = new Chart(ctx, {
             type: 'bar',
             data: barChartData,
             options: {
                 responsive: true,
                 legend: {
                     position: 'top',
                 },
                 title: {
                     display: true,
                     text: 'Active Users'
                 }
             }
         });

  });
</script>