<?php include('includes/server.php');?>
<?php include('header.php') ;?>
<?php
$file='mail';
$files='management';
$result = $db->query('SELECT * FROM sentemail order by id desc')->fetchAll();?>
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Sent Mail List</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Sent Mail List</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
								<div class="card-body">
								
									<div class="row table-padding">
										<div class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											<div class="btn-group pull-right">
												<a href="compose.php" id="addRow" class="btn btn-info">
													Compose Email <i class="fa fa-plus"></i>
												</a>
											</div>
										</div>
									</div>
									<table class="table table-hover table-checkable order-column  "  style="width:100%;" id="example1">
										<thead>
											<tr>
												<th> ID </th>
												<th> Email </th>
												<th> Subject </th>
												<th> Status </th>
												<th> Date Sent </th>
												<th> Action </th>
											</tr>
										</thead>
										<tbody>
											<?php
											if($result) {
												foreach ($result as $data) {
											?>
												<tr class="odd gradeX">
													<td ><?php echo $data['id'];?></td>
													<td ><?php echo $data['email'];?></td>
													<td><?php echo $data['subject'];?></td>
													<td><?php echo ($data['email_sent']) == 1 ? '<span class="label label-sm label-success"> Success </span>' : '<span class="label label-sm label-danger"> Fail </span>' ;?></td>
													<td><?php echo $data['date_added'];?></td>
													<td>
														<a  id = "mybutton_<?php echo $data['id'];?>"   data-id="<?php echo $data['id'];?>"  class="mybutton btn btn-danger btn-xs">
															<i class="fa fa-trash-o "></i>
														</a>
													</td>
												</tr>
											<?php } ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<th> ID </th>
												<th> Email </th>
												<th> Subject </th>
												<th> Status </th>
												<th> Date Sent </th>
												<th> Action </th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
		
<script>
$(document).ready(function(){
	$('.mybutton').click(function(){
		var ID = $(this).data('id');
		$('#confirm-button').data('id', ID); //set the data attribute on the modal button
		showCancelMessage(ID);
	});
	
	function showCancelMessage(deleteid) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to see this email log!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plx!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
				var el = $("#mybutton_"+deleteid);
					$.ajax({
						url: 'deluser.php',
						type: 'POST',
						data: { emailid:deleteid },
						success: function(response){
							// Removing row from HTML Table
							if(response == 1){
								$(el).closest('tr').css('background','tomato');
								$(el).closest('tr').fadeOut(800,function(){
									//$('#confirm_delete_modal').modal('hide');
								});
							}else{
								alert(response);
							}

						}
					});
				swal("Deleted!", "Email has been deleted.", "success");
			} else {
				swal("Cancelled", "Email not delete :)", "error");
			}
		});
	}
	// Delete 
	/*$('#confirm-button').click(function(){
		// Delete id
		var deleteid = $(this).data('id');
		// AJAX Request
		var el = $("#mybutton_"+deleteid);
		$.ajax({
			url: 'deluser.php',
			type: 'POST',
			data: { id:deleteid },
			success: function(response){
				// Removing row from HTML Table
				if(response == 1){
					$(el).closest('tr').css('background','tomato');
					$(el).closest('tr').fadeOut(800,function(){
						$('#confirm_delete_modal').modal('hide');
					});
				}else{
					alert(response);
				}

			}
		});
	});*/
});
$(document).ready(function() {
	'use strict';
	$('#example1').DataTable( {
		"columnDefs": [
				{ "visible": false, "targets": 0 }
		   ],
		   "order": [[ 0, 'desc' ]],
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );
});
</script>