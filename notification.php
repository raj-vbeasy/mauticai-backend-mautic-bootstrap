<?php include('includes/server.php');?>
<?php include('header.php') ;?>
<?php
$file='notification';
$files='notification';
$result = $db->query("SELECT audit_log.date_added,audit_log.object_id,audit_log.user_name,audit_log.action,audit_log.user_id,audit_log.id as nid FROM audit_log inner join users on audit_log.object_id = users.id where bundle = 'user' and object in ('security','user') and action in ('login','create')  order by audit_log.id desc ")->fetchAll();?>
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Notifications</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Notifications List</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="tabbable-line">
								
								<div class="tab-content">
									<div class="tab-pane active fontawesome-demo" id="tab1">
										<div class="row">
											<div class="col-md-12">
												<div class="card card-topline-red">
													<div class="card-body ">

														<div class="table-scrollable">
															<table class="table table-hover table-checkable order-column full-width" id="example4">
																<thead>
																	<tr>
																		<th> ID </th>
																		<th> Email </th>
																		<th> User </th>
																		<th> Log </th>
																		<th> Time </th>
																		<!--th> Action </th -->
																	</tr>
																</thead>
																<tbody>
																	<?php
																	if($result) {
																		foreach ($result as $key => $data) {
																			$result_data = $db->query("SELECT username,email FROM users where id='".$data['object_id']."'")->fetchArray();
																	?>
																		<tr class="gradeX <?php echo ($key%2 ==0) ? "even" : "odd";?> ">
																			<td><?php echo $data['nid'];?></td>
																			<td><?php echo trim($result_data['email']);?></td>
																			<td><?php echo $result_data['username'];?></td>
																			<td><?php echo $data['action'] ;?></td>
																			<td><?php echo $data['date_added'] ;?></td>
																			<!--td>
																			<?php if($data['action'] == "create"){ ?>
																				<a  data-toggle="modal" data-target="#mediumModel"id = "mybutton_<?php echo $data['nid'];?>"   data-id="<?php echo $data['nid'];?>"  class="mybutton btn btn-danger btn-xs">
																						<i class="fa fa-list"></i>
																					</a>
																			<?php } ?>
																			</td -->
																		</tr>
																		<?php } ?>
																	<?php } ?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
<div class="modal fade" id="userdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel">User Details</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>		
<script>
$(document).ready(function(){
	$('.mybutton').click(function(){
		var ID = $(this).data('id');
		$.ajax({
			url: 'getNotificationDetails.php',
			type: 'POST',
			data: { id:ID },
			success: function(response){
				// Removing row from HTML Table
				if(response != 1){
					$("#userdetails").modal("show");
				}else{
					alert(response);
					$("#userdetails").modal("hide");
				}

			}
		});
		
		$('#confirm-button').data('id', ID); //set the data attribute on the modal button
		
	});
});
</script>