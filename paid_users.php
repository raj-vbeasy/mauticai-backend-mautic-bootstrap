<?php include('includes/server.php');
$description = 'Paid Users';
$author = 'Paid Users';
$title = 'Paid Users';
include('header.php') ;?>
<?php
$file='paid_user';
$files='stat';
$result = $db->query('SELECT * FROM users where  stripeid !=""  ')->fetchAll();?>
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Paid Users</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Paid Users List</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
								<div class="card-body">
									<table class="table table-hover table-checkable order-column  "  style="width:100%;" id="example1">
										<thead>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> User Name </th>
												<th> Phone Number </th>
												<th> Payment Date </th>
												<th> Expire Date </th>
												<th>Status</th>
												<th> Action </th>
											</tr>
										</thead>
										
										<tbody>
											<?php
											if($result) {
												foreach ($result as $data) {
											?>
												<tr class="odd gradeX">
													<td><?php echo $data['first_name'].' '.$data['last_name'];?></td>
													<td><?php echo $data['email'];?></td>
													<td><?php echo $data['username'];?></td>
													<td><?php echo $data['phonenumber'];?></td>
													<td><?php echo $data['paymentdate'];?></td>
													<td><?php echo $data['expiredate'];?></td>
													<td><?php echo ($data['is_published']) == 1 ? '<span class="label label-sm label-success"> Active </span>' : '<span class="label label-sm label-danger"> Inactive </span>' ;?></td>
													<td>
														<?php if($data['id'] !=1) { ?>
														<a  id = "mybutton_<?php echo $data['id'];?>"   data-id="<?php echo $data['id'];?>"  class="mybutton btn btn-danger btn-xs">
															<i class="fa fa-trash-o "></i>
														</a>
														<?php } ?>
													</td>
												</tr>
											<?php } ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> User Name </th>
												<th> Phone Number </th>
												<th> Payment Date </th>
												<th> Expire Date </th>
												<th>Status</th>
												<th> Action </th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<?php include('del_model.php') ;?>
		<!-- end page container -->
		<?php include('footer.php') ;?>
		<script>
$(document).ready(function(){
	$('.mybutton').click(function(){
		var ID = $(this).data('id');
		$('#confirm-button').data('id', ID); //set the data attribute on the modal button
		showCancelMessage(ID);
	});
	
	function showCancelMessage(deleteid) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this user!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plx!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
				var el = $("#mybutton_"+deleteid);
					$.ajax({
						url: 'deluser.php',
						type: 'POST',
						data: { id:deleteid },
						success: function(response){
							// Removing row from HTML Table
							if(response == 1){
								$(el).closest('tr').css('background','tomato');
								$(el).closest('tr').fadeOut(800,function(){
									//$('#confirm_delete_modal').modal('hide');
								});
							}else{
								alert(response);
							}

						}
					});
				swal("Deleted!", "Your user  has been deleted.", "success");
			} else {
				swal("Cancelled", "Your user  is safe :)", "error");
			}
		});
	}
	// Delete 
	/*$('#confirm-button').click(function(){
		// Delete id
		var deleteid = $(this).data('id');
		// AJAX Request
		var el = $("#mybutton_"+deleteid);
		$.ajax({
			url: 'deluser.php',
			type: 'POST',
			data: { id:deleteid },
			success: function(response){
				// Removing row from HTML Table
				if(response == 1){
					$(el).closest('tr').css('background','tomato');
					$(el).closest('tr').fadeOut(800,function(){
						$('#confirm_delete_modal').modal('hide');
					});
				}else{
					alert(response);
				}

			}
		});
	});*/
});
$(document).ready(function() {
	'use strict';
	$('#example1').DataTable( {
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );
});
</script>