<?php include('includes/server.php');?>
<?php include('header.php') ;?>
<?php
$file='users';
$result = $db->query("SELECT * FROM admin_users where id='".$_SESSION['userData']['id']."'")->fetchArray();?>
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Profile</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Profile</li>
							</ol>
						</div>
					</div>
					<?php if(isset($profile_error)) { ?>
						<div class="row">
							<div class="col-md-12">
								<?php if($profile_error['error'] == true) { ?>
									<div class="alert alert-danger" role="alert">
										<strong>Error!</strong> <?php echo $profile_error['message'];?>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										  </button>
									</div>
								<?php } ?>
								<?php if($profile_error['error'] == false) { ?>
									<div class="alert alert-success" role="alert">
										<strong>Success!</strong> <?php echo $profile_error['message'];?>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										  </button>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
								<div class="card-head">
									<header>User Information</header>
								</div>
								<div class="card-body" id="bar-parent">
									<form action="profile.php" method="post" id="form_sample_1" class="form-horizontal">
										<div class="form-body">
											<div class="form-group row">
												<label class="control-label col-md-3">First Name
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="first_name" id="first_name" data-required="1" placeholder="enter first name" value="<?php echo $result['first_name'];?>" class="form-control input-height" />
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-3">Last Name
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="last_name" id="last_name" data-required="1" placeholder="enter last name" value="<?php echo $result['last_name'];?>" class="form-control input-height" />
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-3">Email
												</label>
												<div class="col-md-5">
													<div class="input-group">
														<span class="input-group-addon">
														</span>
														<input type="text" id="email" class="form-control input-height" name="email" value="<?php echo $result['email'];?>" placeholder="Email Address">
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-3">Password
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="password" name="password" id="password" data-required="1" placeholder="enter Password" class="form-control input-height" />
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-3">Confirm Password
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="password" name="confirm_password" id="confirm_password" data-required="1" placeholder="Reenter your password" class="form-control input-height" />
												</div>
											</div>											
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="offset-md-3 col-md-9">
													<button type="submit" name="profile_submit" id="profile_submit" class="btn btn-info m-r-20">Update</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
		
<script>
$(document).ready(function(){
	$( "#profile_submit" ).click(function( event ) {
		var first_name = $("#first_name").val().trim();
		var last_name = $("#last_name").val().trim();
		var email = $("#email").val().trim();
		var password = $("#password").val().trim();
		var confirm_password = $("#confirm_password").val().trim();
		if(first_name.length == 0) {
			alert("Enter First Name");
			return false;
		}
		if(last_name.length == 0) {
			alert("Enter Last Name");
			return false;
		}
		if(email.length == 0) {
			alert("Enter Email");
			return false;
		}
		if(password != confirm_password) {
			alert("Password or confirm password does not match");
		}
		return true;
		//event.preventDefault();
	});
});
</script>