<?php
include('includes/server.php');
require 'phpmailer/src/PHPMailer.php'; 
require 'phpmailer/src/SMTP.php'; 
require 'phpmailer/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;

if(isset($_POST['to_user_id'])) {
	$result = $db->query('SELECT id,first_name,last_name,email FROM users where id = ? ',$_POST['to_user_id'])->fetchArray();
	if(isset($result['id'])) {
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host = $smtp_detail['mailer_host'];
		$mail->SMTPAuth = true;
		$mail->Username = $smtp_detail['mailer_user']; 
		$mail->Password = $smtp_detail['mailer_password'];
		$mail->SMTPSecure = $smtp_detail['mailer_encryption'];
		$mail->Port = $smtp_detail['mailer_port'];
		$mail->AuthType = $smtp_detail['mailer_auth_mode'];
		
		$mail->setFrom($smtp_detail['mailer_from_email'], $smtp_detail['mailer_from_name']);
		$mail->addAddress($result['email'], $result['first_name']. ' '.$result['last_name']);
		$mail->Subject = $_POST['subject'];
		$mail->isHTML(true);
		$mail->Body = $_POST['email_body'];
		if($mail->send()){
			echo '1';
		}else{
			echo 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
		}
		exit;
	}
}