<div class="sidebar-container">
	<div class="sidemenu-container navbar-collapse collapse fixed-menu">
		<div id="remove-scroll" class="left-sidemenu">
			<ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false"
				data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
				<li class="sidebar-toggler-wrapper hide">
					<div class="sidebar-toggler">
						<span></span>
					</div>
				</li>
				<li class="nav-item <?php echo ($files=="notification") ? 'active open' : ''; ?>">
					<a href="notification.php" class="nav-link nav-toggle">
						<i class="material-icons">add_alert</i>
						<span class="title">Notification</span>
						<!--span class="label label-rouded label-menu label-success">10</span -->
					</a>
				</li>
				<li class="nav-item <?php echo ($files=="dashboard") ? 'active open' : ''; ?>">
					<a href="index.php" class="nav-link nav-toggle">
						<i class="material-icons">dashboard</i>
						<span class="title">Dashboard</span>
					</a>
				</li>
				<li class="nav-item <?php echo ($files=="stat") ? 'active open' : ''; ?>" >
					<a href="#" class="nav-link nav-toggle"> 
						<i class="material-icons">person</i>
						<span class="title">User Statics</span> <span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li class="nav-item <?php echo ($file=="total_sale") ? 'active open' : ''; ?>">
							<a href="total_sales.php" class="nav-link "> 
								<span class="title">Total Sales</span>
							</a>
						</li>
						<li class="nav-item <?php echo ($file=="paid_user") ? 'active open' : ''; ?>">
							<a href="paid_users.php" class="nav-link "> 
								<span class="title">Paid</span>
							</a>
						</li>
						<li class="nav-item <?php echo ($file=="unpaid_user") ? 'active open' : ''; ?>">
							<a href="unpaid_users.php" class="nav-link "> 
								<span class="title">Unpaid</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="nav-item <?php echo ($files=="management") ? 'active open' : ''; ?>">
					<a href="#" class="nav-link nav-toggle"><i class="material-icons">group</i>
						<span class="title">User Management</span><span class="arrow"></span></a>
					<ul class="sub-menu">
						<li class="nav-item <?php echo ($file=="users") ? 'active open' : ''; ?>">
							<a href="users.php" class="nav-link "> 
								<span class="title">Total Users</span>
							</a>
						</li>
						<li class="nav-item <?php echo ($file=="users_role") ? 'active open' : ''; ?>">
							<a href="users_role.php" class="nav-link "> 
								<span class="title">User Settings</span>
							</a>
						</li>
						<li class="nav-item <?php echo ($file=="mail") ? 'active open' : ''; ?>">
							<a href="mail.php" class="nav-link "> 
								<span class="title">Mail</span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>