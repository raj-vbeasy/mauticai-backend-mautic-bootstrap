<?php include('includes/server.php');
$description = 'Total Sales';
$author = 'Total Sales';
$title = 'Total Sales';
include('header.php') ;?>
<?php
$file='total_sale';
$files='stat';
$result = $db->query('SELECT sum(amountpaid) as amount FROM users where stripeid !="" ')->fetchArray();
$result_by_year_month = $db->query('SELECT sum(amountpaid) as amount, DATE_FORMAT(paymentdate,"%Y") as year,DATE_FORMAT(paymentdate,"%m") as month FROM users where  stripeid !=""  group by  DATE_FORMAT(paymentdate,"%Y"),DATE_FORMAT(paymentdate,"%m") ')->fetchAll();
?>

		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Total Sales</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Total Sales</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
							<div class="row clearfix">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="card">
										<div class="panel-body">
											<h3>Total Payment Received</h3>
											
											<span class="text-small margin-top-10 full-width">AU$ <?php echo number_format($result['amount'],2);?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
								<div class="card-body">
									<table class="table table-hover table-checkable order-column  "  style="width:100%;" id="example1">
										<thead>
											<tr>
												<th> Year </th>
												<th> Month </th>
												<th> Amount </th>
											</tr>
										</thead>
										<tbody>
											<?php
											if($result_by_year_month) {
												foreach ($result_by_year_month as $data) {
											?>
												<tr class="odd gradeX">
													<td><?php echo $data['year'];?></td>
													<td><?php echo $data['month'];?></td>
													<td><?php echo 'AU$ '.number_format($data['amount'],2);?></td>
													
												</tr>
											<?php } ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<th> Year </th>
												<th> Month </th>
												<th> Amount </th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
<script>
$(document).ready(function() {
	'use strict';
	$('#example1').DataTable( {
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );
});
</script>