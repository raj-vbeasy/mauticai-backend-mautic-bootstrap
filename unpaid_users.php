<?php include('includes/server.php');
$description = 'Unpaid Users';
$author = 'Unpaid Users';
$title = 'Unpaid Users';
include('header.php') ;?>
<?php
$file='unpaid_user';
$files='stat';
$result = $db->query('SELECT * FROM users where stripeid is null or stripeid = "" ')->fetchAll();?>
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Unpaid Users</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Unpaid Users List</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
								<div class="card-body">
									<table class="table table-hover table-checkable order-column  "  style="width:100%;"id="example1">
										<thead>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> User Name </th>
												<th> Phone Number </th>
												<th>Status</th>
												<th> Action </th>
											</tr>
										</thead>
										<tbody>
											<?php
											if($result) {
												foreach ($result as $data) {
											?>
												<tr class="odd gradeX">
													<td><?php echo $data['first_name'].' '.$data['last_name'];?></td>
													<td><?php echo $data['email'];?></td>
													<td><?php echo $data['username'];?></td>
													<td><?php echo $data['phonenumber'];?></td>
													<td><?php echo ($data['is_published']) == 1 ? '<span class="label label-sm label-success"> Active </span>' : '<span class="label label-sm label-danger"> Inactive </span>' ;?></td>
													<td>
														<a data-name="<?php echo $data['first_name'].' '.$data['last_name'];?>" data-user = "<?php echo $data['first_name'].' '.$data['last_name']. ' '.$data['email'].' ';?>" data-id="<?php echo $data['id'];?>"  data-toggle="modal" data-target="#sendemail" class="sendemail btn btn-primary  btn-xs">
															<i class="fa fa-send "></i>
														</a>
														<?php if($data['id'] !=1) { ?>
														<a  id = "mybutton_<?php echo $data['id'];?>"   data-id="<?php echo $data['id'];?>"  class="mybutton btn btn-danger btn-xs">
															<i class="fa fa-trash-o "></i>
														</a>
														<?php } ?>
													</td>
												</tr>
											<?php } ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> User Name </th>
												<th> Phone Number </th>
												<th>Status</th>
												<th> Action </th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<?php include('del_model.php') ;?>
		<!-- end page container -->
		
		<?php include('footer.php') ;?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
		
<div class="modal fade" id="sendemail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
 aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLongTitle">Send Email</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="inbox-body">
					<div class="inbox-header"></div>
					<div class="inbox-body no-pad">
						<div class="mail-list">
							<div class="compose-mail">
								<form method="post" id="sent_email" name="sent_email">
									<div class="form-group">
										<label for="to" class="">To:</label>
										<input type="hidden" value="" name="to_user" id="to_user" />
										<input type="hidden" value="" name="to_user_id" id="to_user_id" />
										<span class="compose_to"></span>
									</div>
									<div class="form-group">
										<label for="subject" class="">Subject:</label>
										<input type="text" value="Payment Due" tabindex="1" id="subject" name="subject" class="form-control">
									</div>
									<div class="compose-editor">
										<textarea id="summernote" name="email_body" class="summernote"></textarea>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="sendemailuser" class="btn btn-primary">Send</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('.mybutton').click(function(){
		var ID = $(this).data('id');
		$('#confirm-button').data('id', ID); //set the data attribute on the modal button
		showCancelMessage(ID);
	});

	$('.sendemail').click(function(){
		var ID = $(this).data('id');
		var to_user = $(this).data('user');
		var name = $(this).data('name');
		$("#to_user").val(name);
		$("#to_user_id").val(ID);
		$('#sendemailuser').data('id', ID); //set the data attribute on the modal button
		$('.compose_to').html('<strong>'+to_user+'</strong>');
		var msg = "Hello <strong>"+ name+"</strong>,<Br />";
		msg += "Please make payment to see all your dashboard functionality.<br/>";
		msg += "Here is link to make payment <a target='_new' href='https://app.mauticai.com/stripe'> https://app.mauticai.com/stripe </a>.<br/><br/>";
		msg += "Thanks.<br/>Mauticai Team,<b /><br/>";
		$('#summernote').val(msg);
		$('#summernote').summernote('destroy');
		$('#summernote').summernote({
			placeholder: '',
			tabsize: 2,
			height: 200,
		});
		
	});
	
	$('#sendemailuser').click(function(){
		var form = $("#sent_email");
		
		var subject = $("#subject").val().trim();
		var summernote = $("#summernote").val().trim();

		if(subject.length == 0) {
			alert("Enter Subject");
			return false;
		}
		if(summernote.length == 0) {
			alert("Enter Message Body");
			return false;
		}
		$.LoadingOverlay("show");

		$.ajax({
			url: 'sentemail.php',
			type: 'POST',
			data: form.serialize(),
			success: function(response){
				// Removing row from HTML Table
				if(response == 1){
					swal("Email Sent!", "Email sent to user.", "success");
					$("#sendemail").modal('hide');
				}else{
					$("#sendemail").modal('hide');
					swal("Cancelled", response, "error");
				}
				$.LoadingOverlay("hide",true);
			}
		});
	})
	
	function showCancelMessage(deleteid) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this user!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plx!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
				var el = $("#mybutton_"+deleteid);
					$.ajax({
						url: 'deluser.php',
						type: 'POST',
						data: { id:deleteid },
						success: function(response){
							// Removing row from HTML Table
							if(response == 1){
								$(el).closest('tr').css('background','tomato');
								$(el).closest('tr').fadeOut(800,function(){
									//$('#confirm_delete_modal').modal('hide');
								});
							}else{
								alert(response);
							}

						}
					});
				swal("Deleted!", "Your user  has been deleted.", "success");
			} else {
				swal("Cancelled", "Your user  is safe :)", "error");
			}
		});
	}
});
$(document).ready(function() {
	'use strict';
	$('#example1').DataTable( {
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );
});
</script>