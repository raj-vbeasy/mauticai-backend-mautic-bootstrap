<?php include('includes/server.php');?>
<?php include('header.php') ;?>
<?php
$file='users_role';
$files='management';
$result = $db->query('SELECT * FROM users')->fetchAll();?>
		<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<?php include('sidebar.php'); ?>
			<!-- end sidebar menu -->
			<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Users Settings</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Users Settings</li>
							</ol>
						</div>
					</div>
					<div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
								<div class="card-body">
									<table class="table table-hover table-checkable order-column  "  style="width:100%;" id="example1">
										<thead>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> User Name </th>
												<th> Phone Number </th>
												<th> Subscription Date </th>
												<th> Expire Date </th>
												<th> Amount Paid </th>
												<th>Status</th>
												<th> Enable/Disable</th>
											</tr>
										</thead>
										
										<tbody>
											<?php
											if($result) {
												foreach ($result as $data) {
											?>
												<tr class="odd gradeX">
													<td><?php echo $data['first_name'].' '.$data['last_name'];?></td>
													<td><?php echo $data['email'];?></td>
													<td><?php echo $data['username'];?></td>
													<td><?php echo $data['phonenumber'];?></td>
													<td><?php echo $data['paymentdate'];?></td>
													<td><?php echo $data['expiredate'];?></td>
													<td><?php echo 'AU$. '.number_format($data['amountpaid'],2);?></td>
													<td><?php echo ($data['is_published']) == 1 ? '<span class="label label-sm label-success"> Active </span>' : '<span class="label label-sm label-danger"> Inactive </span>' ;?></td>
													<td>
														<?php if($data['id'] !=1) { ?>
															<?php if($data['is_published'] == 1) { ?>
																<a  title="Disable User" id = "mybutton_<?php echo $data['id'];?>" data-val="<?php echo $data['is_published'];?>"  data-id="<?php echo $data['id'];?>"  class="mybutton btn btn-danger btn-xs">
																	<i class="fa fa-minus "></i>
																</a>
															<?php } else { ?>
																	<a  title="Enable User" id = "mybutton_<?php echo $data['id'];?>"   data-val="<?php echo $data['is_published'];?>"  data-id="<?php echo $data['id'];?>"  class="mybutton btn btn-danger btn-xs">
																	<i class="fa fa-plus"></i>
																</a>
															<?php } ?>
														<?php } ?>
													</td>
												</tr>
											<?php } ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> User Name </th>
												<th> Phone Number </th>
												<th>Status</th>
												<th> Enable/Disable</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
			<?php include('chat_sidebar.php') ;?>
		</div>
		<!-- end page container -->
		<?php include('footer.php') ;?>
		
<script>

$(document).ready(function(){
	$('.mybutton').click(function(){
		var enabledisableid = $(this).data('id');
		var enadisa = $(this).data('val');
		showCancelMessage(enabledisableid,enadisa);
	});
	
	function showCancelMessage(enabledisableid, enadisa) {
		var ms = '';
		if(enadisa == 0 ){
			ms = 'Enable';
		}
		if(enadisa == 1 ){
			ms = 'Disable';
		}
		swal({
			title: "Are you sure?",
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, "+ms+" it!",
			cancelButtonText: "No, cancel plx!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
					// AJAX Request
					$.ajax({
						url: 'deluser.php',
						type: 'POST',
						data: { enabledisable:enabledisableid },
						success: function(response){
							// Removing row from HTML Table
							if(response == 1 || response == 2){
								window.location.href = "users_role.php?status="+response;
							}else{
								alert(response);
							}

						}
					});
				swal(ms, "Your successfully "+ms+" User.", "success");
			} else {
				swal("Cancelled", "", "error");
			}
		});
	}
});
$(document).ready(function() {
	'use strict';
	$('#example1').DataTable( {
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );
});
</script>